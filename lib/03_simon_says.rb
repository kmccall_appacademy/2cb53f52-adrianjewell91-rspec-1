ef echo(str)

  str

end
#########################
def shout(str)

  str.upcase

end
#########################
def repeat(str, final_length=2)

  new_str = str

  (final_length-1).times do

    new_str += " " + str

  end

  new_str

end
#############################
def start_of_word(str,num_of_letters=1)

  str[0..(num_of_letters-1)]

end
##############################
def first_word(str)

  str.split(" ")[0]

end
###############################
def titleize(str)

  arr = str.split(" ")

  arr.each_index do |i|

    if i==0 || i == arr.length-1 || arr[i].length>4
      arr[i] = arr[i].capitalize
    end

  end.join(" ") #In this case, reduce was too cumbersome, each_index was more efficient.

end
